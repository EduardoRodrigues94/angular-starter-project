function svcSnippet($scope){
    return{
        snippetVariable: [],

        snippetFunction: snippetFunction,
    };

    function snippetFunction(args){
        $scope.$emit('onSnippetEmit', args);
    }

}

angular.module('app').factory('svcSnippet', svcSnippet);
