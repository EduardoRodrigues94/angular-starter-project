(function () {

    //Controller Function
    var snippetComponent = function ($scope, $location) {

        var vm = this;
        vm.snippetVariable = [];
        //Function Bindings

        //$onInit
        vm.$onInit = function () {
           
            if(svcSnippet.snippetVariable == undefined)
            {
                $location.path("/");
                return;
            }
        };

        //$on
        $scope.$on('onSnippetEmit', function(args){
            console.log(args);
        });
    };

    //Controller
    angular.module('app').controller('snippetComponent', snippetComponent);

    //Component
    angular.module('app').component('snippetComponent', {
        templateUrl: '../../app/your-path-to-html.html',
        controller: 'snippetComponent',
        controllerAs: 'vm'
    });
})();
